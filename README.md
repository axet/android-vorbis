# libvorbis for Android

Library to encode samples to ogg.

# Natives

Only recent (19+) phones has no issue loading natives. Old phones (16+) unable to locate library dependencies. Very old phones (15+) unable choice proper native library ABI version and crash with error UnsatifiedLinkExcetpion.

  * https://gitlab.com/axet/android-audio-library/blob/master/src/main/java/com/github/axet/audiolibrary/encoders/FormatOGG.java

```java
    import com.github.axet.androidlibrary.app.Native;

    Native.loadLibraries(context, new String[]{"ogg", "vorbis", "vorbisjni"})
```

  * https://gitlab.com/axet/android-library/blob/master/src/main/java/com/github/axet/androidlibrary/app/Native.java


```gradle
dependencies {
    compile 'com.github.axet:vorbis:1.0.5'
}
```

# Compile
    # gradle clean
    # gradle :libogg:publishToMavenLocal :libvorbis:publishToMavenLocal publishToMavenLocal
    # gradle :libogg:publish :libvorbis:publish publish
