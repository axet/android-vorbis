package com.github.axet.vorbisjni;

public class Vorbis {
    static {
        if (Config.natives) {
            System.loadLibrary("vorbisjni");
            Config.natives = false;
        }
    }

    private long handle;

    public native void open(int channels, int sampleRate, float quality);

    public native byte[] encode(short[] buf, int pos, int len);

    public native void close();
}
